﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Serilog;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace TestApp
{
    class Program
    {
        static ILogger logger;

        static void Main(string[] args)
        {
            logger = new LoggerConfiguration()
                .WriteTo.LiterateConsole()
                .MinimumLevel.Verbose()
                .CreateLogger();

            var @event = new Event();
            dynamic expando = new ExpandoObject();
            expando.Title = "TEST";
            logger.Information("{@expando}", expando);


            if (Check.IsDefault(@event, x => x.Id, x => x.Name))
            {
                logger.Information("event has default fields {@event}", @event);
            }


            @event.Id = 20;
            @event.Name = "TEST";

            if (!Check.IsDefault(@event, x => x.Id, x => x.Name))
            {
                logger.Information("event hasn't default fields {@event}", @event);
            }

            var eventSource = new EventSource();
            eventSource.Id = 1;
            eventSource.Name = "TEST";
            eventSource.Events = new List<Event>();

            var eventSource2 = new EventSource();

            eventSource2.Copy(eventSource);

            var cloned = eventSource.CloneDynamicaly() as EventSource;

            eventSource.Assign(nameof(EventSource.Id), 3);
        }
    }
}
