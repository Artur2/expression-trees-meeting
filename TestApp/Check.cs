﻿using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class Check
    {
        static ILogger logger = new LoggerConfiguration()
                .WriteTo.LiterateConsole()
                .MinimumLevel.Verbose()
                .CreateLogger();

        private static ConcurrentDictionary<string, Delegate> _cacheIsDefault = new ConcurrentDictionary<string, Delegate>();

        public static bool IsDefault<T>(T instance, params Expression<Func<T, dynamic>>[] properties) where T : class
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var instanceType = instance.GetType();
            var isDefault = false;

            foreach (var item in properties)
            {
                var memberName = GetMemberName(item.Body);
                var memberType = GetMemberType(item.Body);
                var type = ExtractType(instanceType, memberName, memberType);

                logger.Information(memberName);
                logger.Information("Type {type}", type.Name);

                var key = $"{instanceType.Name}-{memberName}";

                var @delegate = _cacheIsDefault.GetOrAdd(key, (source) =>
                {
                    var parameterExpression = Expression.Parameter(instanceType, "x");
                    var property = Expression.PropertyOrField(parameterExpression, memberName);
                    var equalExpression = Expression.Equal(property, Expression.Default(type));
                    var lambdaExpression = Expression.Lambda(equalExpression, parameterExpression);

                    var compiled = lambdaExpression.Compile();

                    return compiled;
                });

                isDefault |= (bool)@delegate.DynamicInvoke(instance);
            }

            return isDefault;
        }

        public static Type ExtractType(Type instanceType, string propertyName, MemberTypes memberTypes)
        {
            switch (memberTypes)
            {
                case MemberTypes.Field:
                    return instanceType.GetRuntimeField(propertyName).FieldType;
                case MemberTypes.Property:
                    return instanceType.GetRuntimeProperty(propertyName).PropertyType;
                default:
                    throw new InvalidOperationException("Only fields or properties allowed");
            }
        }

        public static string GetMemberName(Expression expression)
        {
            if (expression is UnaryExpression)
            {
                var unary = expression as UnaryExpression;
                var member = unary.Operand as MemberExpression;

                return member.Member.Name;
            }
            else if (expression is MemberExpression)
            {
                return (expression as MemberExpression).Member.Name;
            }

            throw new InvalidOperationException("property or field expression is allowed");
        }

        public static MemberTypes GetMemberType(Expression expression)
        {
            if (expression is UnaryExpression)
            {
                var unary = expression as UnaryExpression;
                var member = unary.Operand as MemberExpression;

                return member.Member.MemberType;
            }
            else if (expression is MemberExpression)
            {
                return (expression as MemberExpression).Member.MemberType;
            }

            throw new InvalidOperationException("property or field expression is allowed");
        }
    }
}
