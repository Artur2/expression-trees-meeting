﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class DeepClonerConfiguration
    {
        internal int MaxLevel { get; set; }

        internal List<Type> CollectionMappers { get; set; }

        internal List<Type> CustomCollectionMappers { get; set; } = new List<Type>();

        public void UseMaxLevel(int maxLevel)
        {
            MaxLevel = maxLevel;
        }

        public void AddCustomCollectionMapper<T>() where T : ICustomCollectionMapper
        {
            CustomCollectionMappers.Add(typeof(T));
        }
    }
}
