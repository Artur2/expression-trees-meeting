﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestApp.Tests
{
    public class ObjectExtensionsTests
    {
        [Fact]
        public void AddToCollection_Must_Correctly_Add()
        {
            var eventSource = new EventSource();
            var @event = new Event();

            eventSource.AddToCollection("Events", @event);

            Assert.Contains(@event, eventSource.Events);
        }

        [Theory]
        [InlineData(1, "TEST")]
        [InlineData(2, "TEST2")]
        public void AddToCollection_Must_Correctly_Add_Multiple_Times(int id, string name)
        {
            var eventSource = new EventSource();
            var @event = new Event()
            {
                Id = id,
                Name = name
            };

            eventSource.AddToCollection("Events", @event);

            Assert.Contains(@event, eventSource.Events);
            Assert.Equal(@event, eventSource.Events.FirstOrDefault(x => x.Id == id));
        }

        [Theory]
        [InlineData(1, "John")]
        [InlineData(2, "Doe")]
        public void RemoveFromCollection_Must_Work_Correctly(int id, string name)
        {
            var eventSource = new EventSource();
            var @event = new Event()
            {
                Id = id,
                Name = name
            };
            var dummyEvent = new Event();


            eventSource.Events.Add(dummyEvent);
            eventSource.Events.Add(@event);

            eventSource.RemoveFromCollecton(nameof(EventSource.Events), @event);

            Assert.Contains(dummyEvent, eventSource.Events);
        }

        [Fact]
        public void RemoveFromCollection_must_throw_argument_null_exception()
        {
            var eventSource = new EventSource();

            Assert.Throws<ArgumentNullException>(() => eventSource.RemoveFromCollecton(null, null));
        }

        [Fact]
        public void AddToCollection_must_throw_Argument_null_exception()
        {
            var eventSource = new EventSource();

            Assert.Throws<ArgumentNullException>(() => eventSource.AddToCollection(null, null));
        }

        [Fact]
        public void Copy_to_must_clone_object_properties()
        {
            var eventSource = new EventSource();
            eventSource.Id = 1;
            eventSource.Name = "TEST";

            var clone = eventSource.CloneDynamicaly() as EventSource;

            Assert.Equal(clone.Id, eventSource.Id);
            Assert.Equal(clone.Name, eventSource.Name);
        }

        [Fact]
        public void Property_assing_must_work_correctly()
        {
            var eventSource = new EventSource();

            eventSource.Assign("Id", 1);

            Assert.Equal(eventSource.Id, 1);
        }

        [Fact]
        public void Property_assign_with_null_parameters_throws_argument_null_exception()
        {
            var eventSource = new EventSource();

            Assert.Throws<ArgumentNullException>(() => eventSource.Assign(null, 1));
            Assert.Throws<ArgumentNullException>(() => eventSource.Assign("Id", null));
        }

        [Fact]
        public void CloneDynamicaly_with_filled_object_must_correcty_return_new_object()
        {
            var eventSource = new EventSource();
            eventSource.Id = 1;
            eventSource.Name = "TEST";

            var clone = eventSource.CloneDynamicaly() as EventSource;

            Assert.Equal(eventSource.Id, clone.Id);
            Assert.Equal(eventSource.Name, clone.Name);

            Assert.False(Object.ReferenceEquals(eventSource, clone));
        }

        [Fact]
        public void Clone_with_null_object_must_throw_exception()
        {
            var eventSource = new EventSource();

            Assert.Throws<ArgumentNullException>(() => eventSource.Copy(null));
        }
    }
}
