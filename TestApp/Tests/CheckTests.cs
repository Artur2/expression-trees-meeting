﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestApp.Tests
{
    public class CheckTests
    {
        [Fact]
        public void IsDefault_With_Null_Insance_Throws_ArgumentNullException()
        {
            var @event = (Event)null;

            Assert.Throws<ArgumentNullException>(() => Check.IsDefault(@event));
        }

        [Fact]
        public void IsDefault_Returns_True_When_Property_Of_Instance_Null()
        {
            var @event = new Event();

            Assert.True(Check.IsDefault(@event, x => x.Id));
        }

        [Fact]
        public void IsDefault_Returns_False_When_All_Properties_Setted()
        {
            var @event = new Event();
            @event.Id = 30;
            @event.Name = "TEST";

            Assert.False(Check.IsDefault(@event, x => x.Id, x => x.Name));
        }
    }
}
