﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public static class ObjectExtensions
    {
        private static ConcurrentDictionary<string, Delegate> _addToCollectionCache = new ConcurrentDictionary<string, Delegate>();

        public static void AddToCollection(this object @this, string collectionName, object value)
        {
            if (string.IsNullOrWhiteSpace(collectionName))
                throw new ArgumentNullException(nameof(collectionName));
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var objectType = @this.GetType();
            var valueType = value.GetType();

            var key = $"{objectType.Name}-{collectionName}";

            var @delegate = _addToCollectionCache.GetOrAdd(key, (sourceKey) =>
            {

                var instanceParameter = Expression.Parameter(objectType, "x");
                var valueParameter = Expression.Parameter(valueType, "p");

                var property = Expression.PropertyOrField(instanceParameter, collectionName);
                var call = Expression.Call(property, "Add", null, valueParameter);

                var lambda = Expression.Lambda(call, instanceParameter, valueParameter);

                return lambda.Compile();
            });

            @delegate.DynamicInvoke(@this, value);
        }

        public static void RemoveFromCollecton(this object @this, string collectionName, object value)
        {
            if (string.IsNullOrWhiteSpace(collectionName))
                throw new ArgumentNullException(nameof(collectionName));
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var objectType = @this.GetType();
            var valueType = value.GetType();

            var key = $"{objectType.Name}-{collectionName}";

            var @delegate = _addToCollectionCache.GetOrAdd(key, (sourceKey) =>
            {

                var instanceParameter = Expression.Parameter(objectType, "x");
                var valueParameter = Expression.Parameter(valueType, "p");

                var property = Expression.PropertyOrField(instanceParameter, collectionName);
                var call = Expression.Call(property, "Remove", null, valueParameter);

                var lambda = Expression.Lambda(call, instanceParameter, valueParameter);

                return lambda.Compile();
            });

            @delegate.DynamicInvoke(@this, value);
        }

        public static object CloneDynamicaly(this object @this)
        {
            var typeOfObject = @this.GetType();

            // Declariation
            /// Parameters
            var source = Expression.Parameter(typeOfObject, "source");
            /// Variables
            var target = Expression.Variable(typeOfObject, "target");
            var label = Expression.Label("target");

            // Assigning new object
            var newObject = Expression.New(typeOfObject);
            var assignNewObject = Expression.Assign(target, newObject);

            var expressions = new List<Expression>();
            expressions.Add(assignNewObject);

            // clonning objects

            foreach (var item in TypeDescriptor.GetProperties(typeOfObject).OfType<PropertyDescriptor>())
            {
                if (item.IsReadOnly)
                {
                    continue;
                }

                var property = Expression.Property(target, item.Name);
                var assignExpression = Expression.Assign(property, Expression.Property(source, item.Name));
                expressions.Add(assignExpression);
            }

            expressions.Add(target);

            var block = Expression.Block(new[] { target }, expressions);

            var lamda = Expression.Lambda(block, source);

            var compiled = lamda.Compile();

            return compiled.DynamicInvoke(@this);
        }

        public static void Copy(this object @this, object from)
        {
            if (from == null)
            {
                throw new ArgumentNullException(nameof(from));
            }

            var typeOfObject = @this.GetType();

            var key = typeOfObject.Name;

            // Declariation
            /// Parameters
            var source = Expression.Parameter(typeOfObject, "source");
            var target = Expression.Parameter(typeOfObject, "target");
            /// Variables

            var expressions = new List<Expression>();

            foreach (var item in TypeDescriptor.GetProperties(typeOfObject).OfType<PropertyDescriptor>())
            {
                var targetProperty = Expression.Property(target, item.Name);
                var sourceProperty = Expression.Property(source, item.Name);
                var assign = Expression.Assign(targetProperty, sourceProperty);
                expressions.Add(assign);
            }

            var block = Expression.Block(expressions);

            var lamda = Expression.Lambda(block, target, source);

            var compiled = lamda.Compile();

            compiled.DynamicInvoke(@this, from);
        }

        public static void Assign(this object @this, string propertyName, object value)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                throw new ArgumentNullException(nameof(propertyName));
            }
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var objectType = @this.GetType();

            var @thisParameter = Expression.Parameter(objectType, "p");
            var valueParameter = Expression.Parameter(value.GetType(), "x");

            var propertyExpression = Expression.Property(thisParameter, propertyName); // p.Id
            var assingExpression = Expression.Assign(propertyExpression, valueParameter); // p.Id = x


            var lambda = Expression.Lambda(assingExpression, thisParameter, valueParameter);

            var compiled = lambda.Compile();

            compiled.DynamicInvoke(@this, value);
        }
    }
}
