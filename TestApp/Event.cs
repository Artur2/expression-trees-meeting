﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class Event
    {
        public int _id;

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
