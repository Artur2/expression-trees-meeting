﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class DeepClonerFactory
    {
        public static DeepCloner New(Action<DeepClonerConfiguration> configurator)
        {
            var configuration = new DeepClonerConfiguration();
            configurator(configuration);

            return new DeepCloner(configuration);
        }
    }
}
