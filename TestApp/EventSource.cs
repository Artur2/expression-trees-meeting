﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    public class EventSource
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Event> Events { get; set; } = new List<Event>();
    }
}
